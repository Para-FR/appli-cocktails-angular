import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PanierComponent} from './panier/panier.component';
import {CocktailsContainerComponent} from './cocktails-container/cocktails-container.component';
import {CocktailDetailsComponent} from './cocktails-container/cocktail-details/cocktail-details.component';
import {CocktailEditComponent} from './cocktails-container/cocktail-edit/cocktail-edit.component';


const routes: Routes = [
  { path: '', redirectTo: 'cocktails', pathMatch: 'full'},
  { path: 'panier', component: PanierComponent },
  { path: 'cocktails', component: CocktailsContainerComponent, children: [
      { path: '', component: CocktailDetailsComponent },
      { path: 'new', component: CocktailEditComponent},
      { path: ':index', component: CocktailDetailsComponent },
      { path: ':index/edit', component: CocktailEditComponent }
    ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
