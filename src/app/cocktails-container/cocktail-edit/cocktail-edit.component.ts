import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import { CocktailService } from '../../shared/services/cocktail.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Cocktail} from '../../shared/cocktail.model';

@Component({
  selector: 'app-cocktail-edit',
  templateUrl: './cocktail-edit.component.html',
  styleUrls: ['./cocktail-edit.component.css']
})
export class CocktailEditComponent implements OnInit {
  public cocktailForm: FormGroup;
  public cocktail: Cocktail;
  public edit: boolean;

  constructor(
      private activatedRoute: ActivatedRoute,
      private fb: FormBuilder,
      private cocktailService: CocktailService
  ) { }

  // On initialise le formulaire et on y ajoute des validators directement ici
  ngOnInit() {
    // On récupère la route actuelle du client
    this.activatedRoute.params.subscribe((params: Params) => {
      // Si il y a un paramètre
      if (params.index) {
        // Si on est mode edit on active notre variable edit
        this.edit = true;
        // On définit this.coktail via la récupération de l'index et on fait appel à notre Service Cocktail pour récupérer notre coktail
        this.cocktailService.getCocktail(params.index)
            .subscribe( (cocktail: Cocktail) => {
              this.cocktail = cocktail;
              // On initialise notre formulaire avec les valeurs du cocktail
              this.initForm(this.cocktail);
            } );
      } else {
        // On désactive le mode edit
        this.edit = false;
        // On va initialiser notre formulaire mais avec des valeurs à null
        this.initForm();
      }
    });
  }
    initForm(cocktail = { name: '', img: '', desc: '', ingredients: []}) {
      this.cocktailForm = this.fb.group({
        name: [cocktail.name, Validators.required],
        img: [cocktail.img, Validators.required],
        desc: [cocktail.desc],
        ingredients: this.fb.array(cocktail.ingredients.map( ingredient => this.fb.group({
          name: [ingredient.name], quantity: [ingredient.quantity]
        })))
      });
    }

  addIngredient(): void {
    (this.cocktailForm.get('ingredients') as FormArray).push(this.fb.group({
      name: [''],
      quantity: ['']
    }));
  }

  createCocktail() {
    if (this.edit) {
      this.cocktailService.editCocktail(this.cocktailForm.value);
    } else {
      this.cocktailService.addCocktail(this.cocktailForm.value);
    }
  }

}
