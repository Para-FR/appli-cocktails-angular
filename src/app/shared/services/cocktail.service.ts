import { Injectable } from '@angular/core';
import { Cocktail } from '../cocktail.model';
import {BehaviorSubject, Observable} from 'rxjs';
import { Ingredient } from '../ingredient.model';
import { HttpClient } from '@angular/common/http';
import {filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CocktailService {
  public cocktails: BehaviorSubject<Cocktail[]> = new BehaviorSubject(null);

  constructor(private http: HttpClient) {
      this.initCocktails();
  }

  // Fonction d'initialisation des cocktils
  initCocktails(): void {
      // Requête de récupération des cocktails en JSON
      this.http.get<Cocktail[]>('https://cocktails-75bc7.firebaseio.com/cocktails.json')
          .subscribe( (cocktails: Cocktail[]) => { // On s'abonne à l'evenement qu'on vient de récupérer
              this.cocktails.next(cocktails); // On met à jour la liste de nos cocktails via la méthode next.
          });
  }

  getCocktail(index: number): Observable<Cocktail> {
      return this.cocktails.pipe(
          filter( (cocktails: Cocktail[]) => cocktails !== null ),
          map( (cocktails: Cocktail[]) => cocktails[index])
      );
  }

  addCocktail(cocktail: Cocktail): void {
      const cocktails = this.cocktails.value.slice();
      cocktails.push(new Cocktail(cocktail.name, cocktail.img, cocktail.desc, cocktail.ingredients.map( ingredient =>
      new Ingredient(ingredient.name, ingredient.quantity)
      )));
      this.cocktails.next(cocktails);
  }

  editCocktail(editCocktail: Cocktail): void {
      // On copie le tableau existant (instanciation)
      const cocktails = this.cocktails.value.slice();
      // On récupère l'index via map et la fonction indexOf (récupération via le nom).
      const index = cocktails.map( c => c.name ).indexOf(editCocktail.name);
      // On remplace notre tableau de cocktails
      cocktails[index] = editCocktail;
      // On met à jour notre liste de cocktails
      this.cocktails.next(cocktails);
      // On met à jour notre liste de cocktails en ligne (firebase)
      this.save();
  }

  // Méthode qui permet de sauvegarder sur l'Api de Firebase
  save(): void {
      // Méthode put qui envoie les données des nouveaux cocktails
      this.http.put('https://cocktails-75bc7.firebaseio.com/cocktails.json', this.cocktails.value).subscribe();
  }
}
